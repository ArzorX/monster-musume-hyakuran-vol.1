;===================================================================================
;基本設定
;===================================================================================

*define

	caption "モンスター娘百覧vol.1"

	nsa			;ＮＳＡアーカイブモード
	filelog			;ファイルアクセスログを取る（ＣＧモード用）
	labellog		;ラベルログを取る（回想モード用）

	textgosub *text_btn	;通常文章のクリック待ちラベル

	globalon		;グローバル変数使用
	usewheel		;ホイール使用
;	useescspc		;ＥＳＣ＆スペースキー使用

;	defaultfont "ＭＳ 明朝"	;テキストフォント
	defaultspeed 20,10,2	;テキストスピード
	maxkaisoupage 200	;回想ページ限界

	rubyon 10,10		;ルビ使用
	shadedistance 1,2


	automode		;オートモード使用

	deletemenu		;メニューバー消去
	savedir "save"		;セーブデータ格納フォルダ指定

	humanz 600		;ウインドウ表示レイヤー
	windowback		;ウインドウ固定表示

	autosaveoff		;セーブ箇所調整用

	kidokuskip		;既読スキップ使用

	bgmfadeout 1000		;ＢＧＭフェードアウト

	loadgosub *load_config	;ロード直後に呼び出すサブルーチン
	effectcut
	mode_wave_demo
	defsub name
	defsub vspl
	defsub cspl
	defsub damage
	defsub damage_nobr
	defsub damage_nobr2
	defsub skillname
	defsub sean_change
	defsub count
	defsub screen_clear
	defsub screen_clear2
	defsub screen_vanish
	defsub screen_appear
	defsub hseanwave
	defsub hseanvoice
	defsub hseanbgm
	defsub skillcount

;===================================================================================
;汎用の変数
;===================================================================================
;各画面で共通して使用している変数
;実際に使用しているのは100-102のみ、他は削除可能

	numalias textkai,0		;改行ｏｒ改ページ
	numalias textlastx,1		;テキスト末尾のＸ座標
	numalias textlasty,2		;テキスト末尾のＹ座標
	numalias bwait,3		;ボタン入力
	numalias menux,4		;メニューＸ座標
	numalias menuy,5		;メニューＹ座標
	numalias sinkoumode,6		;進行モード
	numalias sinkouonoff,7

	numalias creditnow,11		;
	numalias creditx,12		;
	numalias credity,13		;
	numalias creditscroll,14	;

	numalias sub1,101
	numalias sub2,102
	numalias sub3,103
	numalias sub4,104
	numalias sub5,105
	numalias sub6,106
	numalias sub7,107
	numalias sub8,108
	numalias sub9,109
	numalias sub10,110

;===================================================================================
;各画面の変数
;===================================================================================
;※◎印はグローバル変数

;セーブ画面変数---------------------------------------------------

	numalias gamefirst,1501		;◎初回起動
	numalias gameclear,1502		;◎ゲームクリア

	numalias savepagenow,1503	;◎セーブ画面で現在扱っているページ番号
	numalias savenew,    1504	;◎最新のセーブ番号

	numalias savedata,  2000	;◎セーブデータ

	numalias saveorload, 201	;セーブかロードか判断
	numalias savedatanow,202	;現在扱っているデータ番号
	numalias save0or1,   203	;セーブデータの有無確認

	numalias savescreen, 204	;サムネイル画像
	numalias savetextget,205	;セーブテキスト取得
	numalias savenum,    206	;セーブ番号表示
	numalias savetime,   207	;セーブ日時表示
	numalias savetext,   208	;セーブテキスト
	numalias savemonth,  209	;月
	numalias saveday,    210	;日
	numalias savehour,   211	;時
	numalias saveminute, 212	;分

	numalias savepicx,   214	;Ｘ座標基点
	numalias savepicy,   215	;Ｙ座標基点

	numalias savetotal,  216	;セーブ最大数
	numalias savebarone, 217	;バーの最小単位
	numalias savebarone2,218	;バーの表示単位
	numalias savebary,   219	;バーの表示Ｙ座標

;バックログ画面変数-------------------------------------------------------

	numalias backlognow,    241	;現在扱っているバックログ番号
	numalias backloglook,   242	;バックログ内容表示

	numalias backlogtotal,  243	;バックログ合計
	numalias backlogbarone, 244	;バーの最小単位
	numalias backlogbarone2,245	;バーの表示単位
	numalias backlogbary,   246	;バーの表示Ｙ座標

;コンフィグ画面変数-------------------------------------------------------

	numalias textspeed,  1511	;◎テキストスピード
	numalias autotime,   1512	;◎オートモードスピード
	numalias bgmvol,     1513	;◎ＢＧＭボリューム
	numalias sevol,      1514	;◎ＳＥボリューム
	numalias click,      1515	;◎文章表示単位
	numalias skip,       1516	;◎スキップ
	numalias effectspeed,1517	;◎演出速度設定
	numalias windowsize, 1518	;◎ウインドウサイズ
	numalias hseanbgm,   1519	;◎エロシーンＢＧＭ

;ＣＧモード画面変数-------------------------------------------------------

	numalias cgtotal,    261	;ＣＧの枚数
	numalias cgpagetotal,262	;ＣＧの段数（枚数ではない）

	numalias cgpage,     263	;ページ数
	numalias cgbarone,   264	;バーの最小単位
	numalias cgbarone2,  265	;バーの最小単位
	numalias cgbary,     266	;バーの最小単位

	numalias cgname,     267	;ＣＧファイル名
	numalias cgsabun,    268	;ＣＧ表示差分ファイル名

	numalias cgdata001,301
	numalias cgdata002,302
	numalias cgdata003,303
	numalias cgdata004,304
	numalias cgdata005,305
	numalias cgdata006,306
	numalias cgdata007,307
	numalias cgdata008,308
	numalias cgdata009,309
	numalias cgdata010,310
	numalias cgdata011,311
	numalias cgdata012,312
	numalias cgdata013,313
	numalias cgdata014,314
	numalias cgdata015,315
	numalias cgdata016,316
	numalias cgdata017,317
	numalias cgdata018,318
	numalias cgdata019,319
	numalias cgdata020,320
	numalias cgdata021,321
	numalias cgdata022,322
	numalias cgdata023,323
	numalias cgdata024,324
	numalias cgdata025,325
	numalias cgdata026,326
	numalias cgdata027,327
	numalias cgdata028,328
	numalias cgdata029,329
	numalias cgdata030,330
	numalias cgdata031,331
	numalias cgdata032,332
	numalias cgdata033,333
	numalias cgdata034,334
	numalias cgdata035,335
	numalias cgdata036,336
	numalias cgdata037,337
	numalias cgdata038,338
	numalias cgdata039,339
	numalias cgdata040,340
	numalias cgdata041,341
	numalias cgdata042,342
	numalias cgdata043,343
	numalias cgdata044,344
	numalias cgdata045,345
	numalias cgdata046,346
	numalias cgdata047,347
	numalias cgdata048,348
	numalias cgdata049,349
	numalias cgdata050,350
	numalias cgdata051,351
	numalias cgdata052,352
	numalias cgdata053,353
	numalias cgdata054,354
	numalias cgdata055,355
	numalias cgdata056,356
	numalias cgdata057,357
	numalias cgdata058,358
	numalias cgdata059,359
	numalias cgdata060,360
	numalias cgdata061,361
	numalias cgdata062,362
	numalias cgdata063,363
	numalias cgdata064,364
	numalias cgdata065,365
	numalias cgdata066,366
	numalias cgdata067,367
	numalias cgdata068,368
	numalias cgdata069,369
	numalias cgdata070,370
	numalias cgdata071,371
	numalias cgdata072,372
	numalias cgdata073,373
	numalias cgdata074,374
	numalias cgdata075,375
	numalias cgdata076,376
	numalias cgdata077,377
	numalias cgdata078,378
	numalias cgdata079,379
	numalias cgdata080,380
	numalias cgdata081,381
	numalias cgdata082,382
	numalias cgdata083,383
	numalias cgdata084,384
	numalias cgdata085,385
	numalias cgdata086,386
	numalias cgdata087,387
	numalias cgdata088,388
	numalias cgdata089,389
	numalias cgdata090,390
	numalias cgdata091,391
	numalias cgdata092,392
	numalias cgdata093,393
	numalias cgdata094,394
	numalias cgdata095,395
	numalias cgdata096,396
	numalias cgdata097,397
	numalias cgdata098,398
	numalias cgdata099,399
	numalias cgdata100,400


;ＢＧＭモード画面変数-------------------------------------------------------

	numalias bgmtotal,  281		;ＢＧＭの段数

	numalias bgmpage,   282		;ページ数
	numalias bgmbarone, 283		;バーの最小単位
	numalias bgmbarone2,284		;バーの最小単位
	numalias bgmbary,   285		;バーの最小単位

	numalias bgmplaynow,286		;現在再生中のＢＧＭ

	numalias bgmname001,401
	numalias bgmname002,402
	numalias bgmname003,403
	numalias bgmname004,404
	numalias bgmname005,405
	numalias bgmname006,406
	numalias bgmname007,407
	numalias bgmname008,408
	numalias bgmname009,409
	numalias bgmname010,410
	numalias bgmname011,411
	numalias bgmname012,412
	numalias bgmname013,413
	numalias bgmname014,414
	numalias bgmname015,415
	numalias bgmname016,416
	numalias bgmname017,417
	numalias bgmname018,418
	numalias bgmname019,419
	numalias bgmname020,420
	numalias bgmname021,421
	numalias bgmname022,422
	numalias bgmname023,423
	numalias bgmname024,424
	numalias bgmname025,425
	numalias bgmname026,426
	numalias bgmname027,427
	numalias bgmname028,428
	numalias bgmname029,429
	numalias bgmname030,430
	numalias bgmname031,431
	numalias bgmname032,432
	numalias bgmname033,433
	numalias bgmname034,434
	numalias bgmname035,435
	numalias bgmname036,436
	numalias bgmname037,437
	numalias bgmname038,438
	numalias bgmname039,439
	numalias bgmname040,440
	numalias bgmname041,441
	numalias bgmname042,442
	numalias bgmname043,443
	numalias bgmname044,444
	numalias bgmname045,445
	numalias bgmname046,446
	numalias bgmname047,447
	numalias bgmname048,448
	numalias bgmname049,449
	numalias bgmname050,450

	numalias bgmdata001,501
	numalias bgmdata002,502
	numalias bgmdata003,503
	numalias bgmdata004,504
	numalias bgmdata005,505
	numalias bgmdata006,506
	numalias bgmdata007,507
	numalias bgmdata008,508
	numalias bgmdata009,509
	numalias bgmdata010,510
	numalias bgmdata011,511
	numalias bgmdata012,512
	numalias bgmdata013,513
	numalias bgmdata014,514
	numalias bgmdata015,515
	numalias bgmdata016,516
	numalias bgmdata017,517
	numalias bgmdata018,518
	numalias bgmdata019,519
	numalias bgmdata020,520
	numalias bgmdata021,521
	numalias bgmdata022,522
	numalias bgmdata023,523
	numalias bgmdata024,524
	numalias bgmdata025,525
	numalias bgmdata026,526
	numalias bgmdata027,527
	numalias bgmdata028,528
	numalias bgmdata029,529
	numalias bgmdata030,530
	numalias bgmdata031,531
	numalias bgmdata032,532
	numalias bgmdata033,533
	numalias bgmdata034,534
	numalias bgmdata035,535
	numalias bgmdata036,536
	numalias bgmdata037,537
	numalias bgmdata038,538
	numalias bgmdata039,539
	numalias bgmdata040,540
	numalias bgmdata041,541
	numalias bgmdata042,542
	numalias bgmdata043,543
	numalias bgmdata044,544
	numalias bgmdata045,545
	numalias bgmdata046,546
	numalias bgmdata047,547
	numalias bgmdata048,548
	numalias bgmdata049,549
	numalias bgmdata050,550


;回想モード画面変数-------------------------------------------------------

	numalias scenetotal,    601	;回想の枚数
	numalias scenepagetotal,602	;回想の段数（枚数ではない）

	numalias scenepage,     603	;ページ数
	numalias scenebarone,   604	;バーの最小単位
	numalias scenebarone2,  605	;バーの最小単位
	numalias scenebary,     606	;バーの最小単位

	numalias sceneon,       607	;回想モードか判断


;選択肢画面変数-------------------------------------------------------

	numalias selectnum ,681
	numalias selecttext,682
	numalias select00  ,683
	numalias select01  ,684
	numalias select02  ,685
	numalias select03  ,686
	numalias select04  ,687

;===================================================================================
;シナリオの変数
;===================================================================================

	numalias mylife,		801	;自分ＨＰ
	numalias enemylife,		802	;敵ＨＰ
	numalias damage,		803	;ダメージ
	numalias hanyo1,		804	;汎用１
	numalias hanyo2,		805	;汎用２
	numalias hanyo3,		806	;汎用３
	numalias hanyo4,		807	;汎用４
	numalias hanyo5,		808	;汎用５
	numalias hanyo6,		809	;汎用６
	numalias monster_x,		810	;敵立ち絵のＸ座標
	numalias monster_y,		811	;敵立ち絵のＹ座標
	numalias sentaku,		812	;選択肢固有値
	numalias cmd1,			813	;コマンド選択
	numalias kousoku,		814	;拘束
	numalias status,		815	;１恍惚
	numalias sinkou,		816	;バトル進行度
	numalias ransu,			817	;乱数
	numalias ren,			818	;同じ敵セリフが連続で出ないように
	numalias sel_hphalf,		819	;ルカＨＰ半分時のセリフ１回のみ
	numalias sel_kiki,		820	;ルカ射精前のセリフ１回のみ
	numalias max_mylife,		821	;自分最大ＨＰ
	numalias max_enemylife,		822	;敵最大ＨＰ
	numalias bougyo,		823	;防御しているかどうか
	numalias gage,			824	;ＨＰゲージ番号
	numalias kousan,		825	;１こうさん・２おねだり
	numalias move,			826	;スプライト移動用
	numalias zmonster_x,		827	;バッドエンドＸ座標
	numalias battlebgm,		828	;
	numalias mylv,			829	;レベル
	numalias abairitu,		830	;技の攻撃倍率
	numalias stopcmd1,		831	;「こうげき」は有効か
	numalias stopcmd2,		832	;「もがく」は有効か
	numalias stopcmd3,		833	;「ぼうぎょ」は有効か
	numalias stopcmd4,		834	;「なすがまま」は有効か
	numalias stopcmd5,		835	;「こうさん」は有効か
	numalias stopcmd6,		836	;「おねだり」は有効か
	numalias stopcmd7,		837	;「わざ」は有効か
	numalias exp,			838	;経験値
	numalias getexp,		839	;獲得経験値
	numalias mylv_before,		840	;レベルアップ計算用
	numalias mp,			841	;ＭＰ
	numalias max_mp,		842	;最大ＭＰ
	numalias music,			843	;
	numalias end_n,			844	;バッドエンドナンバー
	numalias exp_minus,		845	;引き返す際の経験値マイナス
	numalias genzaiti,		846	;現在地
	numalias a_effect1,		847	;攻撃を受けた時のエフェクト
	numalias a_effect2,		848	;攻撃を与えた時のエフェクト
	numalias mogaku,		849	;もがく回数
	numalias counter,		850	;カウンター状態
	numalias tikei,			851	;地形
	numalias face,			852	;表情
	numalias ransu2,		853	;乱数
	numalias name_count,		854	;ネームプレート文字数
	numalias viclose,		855	;勝敗時のアナウンス
	numalias owaza_count1a,		856	;大技カウント１
	numalias owaza_count1b,		857	;大技カウント２
	numalias owaza_count1c,		858	;大技カウント３
	numalias owaza_count2a,		859	;大技カウント１
	numalias owaza_count2b,		860	;大技カウント２
	numalias owaza_count2c,		861	;大技カウント３
	numalias owaza_count3a,		862	;大技カウント１
	numalias owaza_count3b,		863	;大技カウント２
	numalias owaza_count3c,		864	;大技カウント３
	numalias damage_keigen,		865	;防御力
	numalias teigi1,		866	;ユーザー命令定義用
	numalias teigi2,		867	;ユーザー命令定義用
	numalias teigi3,		868	;ユーザー命令定義用
	numalias teigi4,		869	;ユーザー命令定義用
	numalias teigi5,		870	;ユーザー命令定義用
	numalias teigi6,		871	;ユーザー命令定義用
	numalias teigi7,		872	;ユーザー命令定義用
	numalias teigi8,		873	;ユーザー命令定義用
	numalias teigi9,		874	;ユーザー命令定義用
	numalias list_num1,		875	;リスト１
	numalias list_num2,		876	;リスト２
	numalias list_num3,		877	;リスト３
	numalias list_num4,		878	;リスト４
	numalias list_num5,		879	;リスト５
	numalias list_num6,		880	;リスト６
	numalias list_num7,		881	;リスト７
	numalias list_num8,		882	;リスト８
	numalias list_num9,		883	;リスト９
	numalias list_num10,		884	;リスト１０
	numalias list_num11,		885	;リスト１１
	numalias list_num12,		886	;リスト１２
	numalias finish1,		887	;フィニッシュ時の属性１
	numalias finish2,		888	;フィニッシュ時の属性２
	numalias page,			889	;ページ
	numalias zukan_hyouzyo,		890	;図鑑表情
	numalias zukan_pozu,		891	;図鑑ポーズ
	numalias saveloadoff,		892	;セーブロードオフ
	numalias kaisetu_page,		893	;解説ページ
	numalias kaisetu_pagenum,	894	;解説ページ


	numalias zukan_bukkake2,	894	;ぶっかけ２
	numalias zukan_bukkake3,	895	;ぶっかけ３
	numalias zukan_bukkake4,	896	;ぶっかけ４
	numalias zukan_bukkake5,	897	;ぶっかけ５
	numalias zukan_bukkake6,	898	;ぶっかけ６
	numalias zukan_bukkake7,	899	;ぶっかけ７
	numalias zukan_bukkake8,	900	;ぶっかけ８

	numalias battle,		901	;戦闘中か通常か（メッセージオート用）
	numalias zukan_bukkakenum,	902	;図鑑ぶっかけ数
	numalias zukan_hyouzyonum,	903	;図鑑表情数
	numalias zukan_pozunum,		904	;図鑑ポーズ数
	numalias zukan_sentounum,	905	;図鑑戦闘数
	numalias zukan_ryozyokunum,	906	;図鑑陵辱数
	numalias zukanon,		907	;図鑑オン
	numalias storyon,		908	;ストーリーオン
	numalias manuoff,		909	;メニューオフ
	numalias nanido,		910	;難易度、回想の種類選択時にも流用
	numalias genmogaku,		911	;現在のもがく回数
	numalias henkahp1,		912	;パターン変化時のＨＰ減少値
	numalias henkahp2,		913	;パターン変化時のＨＰ減少値
	numalias henkahp3,		914	;パターン変化時のＨＰ減少値
	numalias status_turn,		915	;ステータス異常持続ターン
	numalias turn,			916	;現在のターン数
	numalias nobtn,			917	;常駐ボタン非表示
	numalias first_sel1,		918	;技用初セリフ１
	numalias first_sel2,		919	;技用初セリフ２
	numalias first_sel3,		920	;技用初セリフ３
	numalias first_sel4,		921	;技用初セリフ４
	numalias zukan_x0,		922	;図鑑Ｘ座標
	numalias zukan_x1,		923	;図鑑Ｘ座標・左
	numalias zukan_x2,		924	;図鑑Ｘ座標・立ち絵
	numalias zukan_koumoku,		925	;図鑑項目
	numalias zukan_skillc1,		926	;図鑑用スキルチェック
	numalias zukan_skillc2,		927	;図鑑用スキルチェック
	numalias zukan_skillc3,		928	;図鑑用スキルチェック
	numalias zukan_skillc4,		929	;図鑑用スキルチェック
	numalias zukan_skillc5,		930	;図鑑用スキルチェック
	numalias zukan_skillc6,		931	;図鑑用スキルチェック
	numalias zukan_skillc7,		932	;図鑑用スキルチェック
	numalias zukan_skillc8,		933	;図鑑用スキルチェック
	numalias zukan_skillc9,		934	;図鑑用スキルチェック
	numalias zukan_skillc10,	935	;図鑑用スキルチェック
	numalias zukan_skillc11,	936	;図鑑用スキルチェック
	numalias zukan_skillc12,	937	;図鑑用スキルチェック
	numalias skill_page,		938	;スキルウィンドウのページ
	numalias wind_turn,		939	;風のターン
	numalias earth_turn,		940	;土のターン
	numalias aqua_turn,		941	;水のターン
	numalias fire_turn,		942	;火のターン
	numalias wind,			943	;風
	numalias earth,			944	;土
	numalias aqua,			945	;水
	numalias fire,			946	;火
	numalias continue,		947	;コンティニュー
	numalias kousoku_maxhp,		948	;もがくダメージ
	numalias kousoku_hp,		949	;もがくダメージ
	numalias story_sinkou,		950	;ストーリー進行度
	numalias damage_nobr,		951	;
	numalias wind_kakuritu,		952	;風の回避率番号
	numalias wind_guard_on,		953	;風回避発動するか
	numalias ikigoe,		954	;イキ声
	numalias nodamage,		955	;ノーダメージ判定用
	numalias aqua_end,		956	;明鏡止水終わり
	numalias kaihi,			957	;回避率
	numalias kadora,		958	;カドラプルギガ発動
	numalias kadora_damage,		959	;カドラプルギガ発動中ダメージ
	numalias earth_keigen,		960	;ノームのダメージ軽減率
	numalias mylv_sin,		961	;レベルドレイン用本来のレベル
	numalias enemylv,		962	;レベルドレイン用敵レベル
	numalias damage_kotei,		963	;ダメージ固定
	numalias owaza_num1,		964	;大技番号
	numalias owaza_num2,		965	;大技番号
	numalias before_action,		966	;事前の行動
	numalias skill_sub,		967	;スキルサブ扱い
	numalias nikaime,		968	;二回攻撃二回目
	numalias nowmusic,		969	;現在の音楽
	numalias hsean,			970	;Ｈシーン
	numalias hseanwave,		971	;Ｈシーン効果音
	numalias txtwindow,		972	;テキストウキンドウ
	numalias hseanvoice,		973	;Ｈシーンフェラボイス
	numalias tukix,			974	;突きＸ座標
	numalias tukiy,			975	;突きＹ座標
	numalias ryozyoku1,		976	;図鑑用陵辱回想１
	numalias ryozyoku2,		977	;図鑑用陵辱回想２
	numalias ryozyoku3,		978	;図鑑用陵辱回想３
	numalias zukancg,		979	;図鑑用ＣＧ
	numalias zukanreturnoff,	980	;図鑑回想スキップオフ
	numalias skill_num,		993	;敵スキル番号
	numalias short_page,		994	;
	numalias hbgmnow,		995	;


;===================================================================================
;グローバル変数（引き継ぎセーブ）
;===================================================================================

	numalias gloval1_story_sinkou,	1601
	numalias gloval1_mylv,		1602
	numalias gloval1_exp,		1603
	numalias gloval1_ivent01,	1604
	numalias gloval1_ivent02,	1605
	numalias gloval1_ivent03,	1606
	numalias gloval1_ivent04,	1607
	numalias gloval1_ivent05,	1608
	numalias gloval1_ivent06,	1609
	numalias gloval1_ivent07,	1610
	numalias gloval1_ivent08,	1611
	numalias gloval1_ivent09,	1612
	numalias gloval1_ivent10,	1613
	numalias gloval1_ivent11,	1614
	numalias gloval1_ivent12,	1615
	numalias gloval1_ivent13,	1617
	numalias gloval1_ivent14,	1618
	numalias gloval1_ivent15,	1619
	numalias gloval1_ivent16,	1620
	numalias gloval1_ivent17,	1621
	numalias gloval1_ivent18,	1622
	numalias gloval1_ivent19,	1623
	numalias gloval1_ivent20,	1624
	numalias gloval1_check01,	1625
	numalias gloval1_check02,	1626
	numalias gloval1_check03,	1627
	numalias gloval1_check04,	1628
	numalias gloval1_check05,	1629
	numalias gloval1_check06,	1630
	numalias gloval1_check07,	1631
	numalias gloval1_check08,	1632
	numalias gloval1_check09,	1633
	numalias gloval1_check10,	1634
	numalias gloval1_check11,	1635
	numalias gloval1_check12,	1636
	numalias gloval1_check13,	1637
	numalias gloval1_check14,	1638
	numalias gloval1_check15,	1639
	numalias gloval1_check16,	1640
	numalias gloval1_check17,	1641
	numalias gloval1_check18,	1642
	numalias gloval1_check19,	1643
	numalias gloval1_check20,	1644
	numalias gloval1_skill01,	1645
	numalias gloval1_skill02,	1646
	numalias gloval1_skill03,	1647
	numalias gloval1_skill04,	1648
	numalias gloval1_skill05,	1649
	numalias gloval1_skill06,	1650
	numalias gloval1_skill07,	1651
	numalias gloval1_skill08,	1652
	numalias gloval1_skill09,	1653
	numalias gloval1_skill10,	1654
	numalias gloval1_item01,	1655
	numalias gloval1_item02,	1656
	numalias gloval1_item03,	1657
	numalias gloval1_item04,	1658
	numalias gloval1_item05,	1659
	numalias gloval1_item06,	1660
	numalias gloval1_item07,	1661
	numalias gloval1_item08,	1662
	numalias gloval1_item09,	1663
	numalias gloval1_item10,	1664
	numalias gloval1_item11,	1665
	numalias gloval1_item12,	1666
	numalias gloval1_item13,	1667
	numalias gloval1_item14,	1668
	numalias gloval1_item15,	1669
	numalias gloval1_item16,	1670
	numalias gloval1_item17,	1671
	numalias gloval1_item18,	1672
	numalias gloval1_item19,	1673
	numalias gloval1_item20,	1674
	numalias gloval1_item21,	1675
	numalias gloval1_item22,	1676
	numalias gloval1_item23,	1677
	numalias gloval1_item24,	1678
	numalias gloval1_item25,	1679
	numalias gloval1_item26,	1680
	numalias gloval1_item27,	1681
	numalias gloval1_item28,	1682
	numalias gloval1_item29,	1683
	numalias gloval1_item30,	1684
	numalias gloval1_nanido,	1685

	numalias gloval2_story_sinkou,	1701
	numalias gloval2_mylv,		1702
	numalias gloval2_exp,		1703
	numalias gloval2_ivent01,	1704
	numalias gloval2_ivent02,	1705
	numalias gloval2_ivent03,	1706
	numalias gloval2_ivent04,	1707
	numalias gloval2_ivent05,	1708
	numalias gloval2_ivent06,	1709
	numalias gloval2_ivent07,	1710
	numalias gloval2_ivent08,	1711
	numalias gloval2_ivent09,	1712
	numalias gloval2_ivent10,	1713
	numalias gloval2_ivent11,	1714
	numalias gloval2_ivent12,	1715
	numalias gloval2_ivent13,	1717
	numalias gloval2_ivent14,	1718
	numalias gloval2_ivent15,	1719
	numalias gloval2_ivent16,	1720
	numalias gloval2_ivent17,	1721
	numalias gloval2_ivent18,	1722
	numalias gloval2_ivent19,	1723
	numalias gloval2_ivent20,	1724
	numalias gloval2_check01,	1725
	numalias gloval2_check02,	1726
	numalias gloval2_check03,	1727
	numalias gloval2_check04,	1728
	numalias gloval2_check05,	1729
	numalias gloval2_check06,	1730
	numalias gloval2_check07,	1731
	numalias gloval2_check08,	1732
	numalias gloval2_check09,	1733
	numalias gloval2_check10,	1734
	numalias gloval2_check11,	1735
	numalias gloval2_check12,	1736
	numalias gloval2_check13,	1737
	numalias gloval2_check14,	1738
	numalias gloval2_check15,	1739
	numalias gloval2_check16,	1740
	numalias gloval2_check17,	1741
	numalias gloval2_check18,	1742
	numalias gloval2_check19,	1743
	numalias gloval2_check20,	1744
	numalias gloval2_skill01,	1745
	numalias gloval2_skill02,	1746
	numalias gloval2_skill03,	1747
	numalias gloval2_skill04,	1748
	numalias gloval2_skill05,	1749
	numalias gloval2_skill06,	1750
	numalias gloval2_skill07,	1751
	numalias gloval2_skill08,	1752
	numalias gloval2_skill09,	1753
	numalias gloval2_skill10,	1754
	numalias gloval2_item01,	1755
	numalias gloval2_item02,	1756
	numalias gloval2_item03,	1757
	numalias gloval2_item04,	1758
	numalias gloval2_item05,	1759
	numalias gloval2_item06,	1760
	numalias gloval2_item07,	1761
	numalias gloval2_item08,	1762
	numalias gloval2_item09,	1763
	numalias gloval2_item10,	1764
	numalias gloval2_item11,	1765
	numalias gloval2_item12,	1766
	numalias gloval2_item13,	1767
	numalias gloval2_item14,	1768
	numalias gloval2_item15,	1769
	numalias gloval2_item16,	1770
	numalias gloval2_item17,	1771
	numalias gloval2_item18,	1772
	numalias gloval2_item19,	1773
	numalias gloval2_item20,	1774
	numalias gloval2_item21,	1775
	numalias gloval2_item22,	1776
	numalias gloval2_item23,	1777
	numalias gloval2_item24,	1778
	numalias gloval2_item25,	1779
	numalias gloval2_item26,	1780
	numalias gloval2_item27,	1781
	numalias gloval2_item28,	1782
	numalias gloval2_item29,	1783
	numalias gloval2_item30,	1784
	numalias gloval2_nanido,	1785


;===================================================================================
;グローバル変数
;===================================================================================

	numalias game_clear,		1800	;ゲームクリア
	numalias taiken_check,		1801	;体験版チェック
	numalias hsean_alice2,		1802	;Ｈシーンフラグ
	numalias hsean_alice3,		1803	;Ｈシーンフラグ
	numalias hsean_alice4,		1804	;Ｈシーンフラグ
	numalias hsean_alice5,		1805	;Ｈシーンフラグ
	numalias hsean_meia1,		1806	;Ｈシーンフラグ
	numalias hsean_sara1,		1807	;Ｈシーンフラグ

	numalias battle_auto,		1850	;戦闘時オート
	numalias hosyoku_cut,		1851	;捕食カット
	numalias hsean_cut,		1852	;エロシーンカット
	numalias zukan_page,		1853	;図鑑ページ
	numalias window_type,		1854	;ウィンドウタイプ
	numalias count_mylv0,		1855	;
	numalias con_hbgm,		1856	;コンフィグ・ＨシーンＢＧＭ
	numalias con_hse,		1857	;コンフィグ・ＨシーンＳＥ
	numalias con_hvoice,		1858	;コンフィグ・Ｈシーンボイス

;2000システム使用、欠番

	numalias count_ko,		2001	;倒した敵の数
	numalias count_end,		2002	;イかされた回数
	numalias count_kousan,		2003	;こうさんした回数
	numalias count_onedari,		2004	;おねだりした回数




;===================================================================================
;シナリオの文字変数
;===================================================================================

	numalias name,			1300	;敵の名前
	numalias haikei,		1301	;戦闘背景画像
	numalias skillname,		1302	;スキル名
	numalias gagenum,		1303	;ＨＰゲージ番号（文字列化）
	numalias bad1,			1304	;バッドエンド１行目
	numalias bad2,			1305	;バッドエンド２行目
	numalias sel1,			1306	;敵セリフ１
	numalias sel2,			1307	;敵セリフ２
	numalias sel3,			1308	;敵セリフ３
	numalias sel4,			1309	;敵セリフ４
	numalias sel5,			1310	;敵セリフ５
	numalias sel6,			1311	;敵セリフ６
	numalias list1,			1312	;リスト１
	numalias list2,			1313	;リスト２
	numalias list3,			1314	;リスト３
	numalias list4,			1315	;リスト４
	numalias list5,			1316	;リスト５
	numalias list6,			1317	;リスト６
	numalias list7,			1318	;リスト７
	numalias list8,			1319	;リスト８
	numalias list9,			1320	;リスト９
	numalias list10,		1321	;リスト１０
	numalias list11,		1322	;リスト１１
	numalias list12,		1323	;リスト１２
	numalias tatie1,		1324	;戦闘立ち絵（普通）
	numalias tatie2,		1325	;戦闘立ち絵（優勢）
	numalias tatie3,		1326	;戦闘立ち絵（劣勢）
	numalias tatie4,		1327	;戦闘立ち絵（拘束）
	numalias platename,		1328	;ネームプレート
	numalias statusnum,		1329	;フェイスアイコン用
	numalias hp_hyouzi,		1330	;ＨＰ表示用
	numalias max_mylifeh,		1331	;ＨＰ表示用
	numalias mylifeh,		1332	;ＨＰ表示用
	numalias menu_mylv,		1333	;メニュー画面（レベル）
	numalias menu_hp,		1334	;メニュー画面（ＨＰ）
	numalias menu_at,		1335	;メニュー画面（攻撃力）
	numalias menu_sp,		1336	;メニュー画面（ＳＰ）
	numalias lavel,			1337	;ラベル
	numalias page_h,		1338	;ページ数表示用
	numalias menu_list1,		1339	;リスト表示用１
	numalias menu_list2,		1340	;リスト表示用２
	numalias menu_list3,		1341	;リスト表示用３
	numalias menu_list4,		1342	;リスト表示用４
	numalias menu_list5,		1343	;リスト表示用５
	numalias menu_list6,		1344	;リスト表示用６
	numalias menu_list7,		1345	;リスト表示用７
	numalias menu_list8,		1346	;リスト表示用８
	numalias menu_list9,		1347	;リスト表示用９
	numalias menu_list10,		1348	;リスト表示用１０
	numalias menu_list11,		1349	;リスト表示用１１
	numalias menu_list12,		1350	;リスト表示用１２
	numalias menu_list13,		1351	;リスト表示用１３
	numalias half_s1,		1352	;ＨＰ半減台詞１
	numalias half_s2,		1353	;ＨＰ半減台詞２
	numalias kiki_s1,		1354	;ＨＰ危機台詞１
	numalias kiki_s2,		1355	;ＨＰ危機台詞２
	numalias zlavel,		1356	;図鑑用ラベル
	numalias zname,			1357	;図鑑用名前
	numalias zukan_lv,		1372	;図鑑用レベル
	numalias zukan_hp,		1373	;図鑑用ＨＰ
	numalias kaisetu_pagec,		1374	;解説ページ
	numalias kaisetu_pagenumc,	1375	;解説ページ
	numalias zukan_ko,		1386	;図鑑用イかされた回数
	numalias end_go,		1387	;コンティニュー後
	numalias hanlist1,		1388	;リスト１
	numalias hanlist2,		1389	;リスト２
	numalias hanlist3,		1390	;リスト３
	numalias hanlist4,		1391	;リスト４
	numalias hanlist5,		1392	;リスト５
	numalias hanlist6,		1393	;リスト６
	numalias hanlist7,		1394	;リスト７
	numalias hanlist8,		1395	;リスト８
	numalias name2,			1396	;敵の名前２
	numalias short_h,		1397	;

	numalias count_mostname,	1600	;最もイかされた敵の名前

;2000は超えない
